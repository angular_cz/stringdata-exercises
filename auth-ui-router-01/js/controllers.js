angular.module('authApp')

    .controller('OrderListController', function(Orders, authService) {
      var orderCtrl = this;
      this.auth = authService;
      this.orders = Orders.query();

      this.statuses = {
        NEW: 'Nová',
        CANCELLED: 'Zrušená',
        PAID: 'Zaplacená',
        SENT: 'Odeslaná'
      };

      this.removeOrder = function(order) {
        order.$remove(function() {
          var index = orderCtrl.orders.indexOf(order);
          orderCtrl.orders.splice(index, 1);
        });
      };


      this.updateOrder = function(order) {
        order.$save();
      };

    })

    .controller('OrderDetailController', function(orderData) {
      this.order = orderData;
    })

    .controller('OrderCreateController', function(orderData, $state) {
      this.order = orderData;

      this.save = function() {
        this.order.$save(function() {
          $state.go("orders.list");
        });
      };
    })

    .controller('HomeCtrl', function(loginModal) {
      this.showLoginModal = function() {
        loginModal.getLoginModal();
      };
    })

    .controller('AuthCtrl', function($scope, $modalInstance, authService) {

      $scope.$on('authService:loginFailed', function() {
        this.message = 'Přihlášení selhalo';
      }.bind(this));

      this.clearMessage = function() {
        this.message = null;
      };

      this.clearMessage();

      this.loginUser = function() {
        this.clearMessage();
        authService.login("user", "password")
            .then(function(auth) {
              $modalInstance.close(auth);
            });
      };

      this.loginAdmin = function() {
        this.clearMessage();
        authService.login("operator", "password")
            .then(function(auth) {
              $modalInstance.close(auth);
            });
        ;
      };

      this.loginFail = function() {
        this.clearMessage();
        authService.login("badUser", "badPassword");
      };

      this.cancel = function() {
        $modalInstance.dismiss('cancel');
      };
    });