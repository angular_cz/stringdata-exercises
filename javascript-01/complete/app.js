function generator(start) {
    var current = start;

    return function() {
        return ++current;
    };
}

var gen1 = generator(0);
console.log(gen1());
console.log(gen1());
console.log(gen1());

/**
 * @param {type} initialNumber
 * @returns {Generator}
 */
var Generator = function(initialNumber) {
    this.number = initialNumber;
    this.getNext = function() {
        return ++this.number;
    };
};

var gen2 = new Generator(10);
console.log(gen2.getNext());
console.log(gen2.getNext());
console.log(gen2.getNext());

getNext = gen2.getNext;
console.log(getNext());  // NaN

obj = {
  number: 365
};

var getNextWithObj = getNext.bind(obj);
console.log(getNextWithObj()); 
