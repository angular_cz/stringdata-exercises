Templates-01

Vyzkoušejte si různé formy expression.

1. Přidejte direktivu ng-app do tagu body nebo html
  ... zdrojový kód TODO 1 -->
                                                                                                          <body class="container" ng-app>

2. Vyzkoušejte zápis výrazů
 - napsat matematický výraz
   ... zdrojový kód TODO 2 -->
                                                                                                          <li>1 + 3 = {{1 + 3}}</li>
                                                                                                          <li>1 * 5 = {{1 * 5}}</li>
 - výraz s ternárním operátorem
   ... zdrojový kód TODO 2 -->
                                                                                                          <li>ternary-true: {{3 == '3' ? 'ano' : 'ne'}}</li>
                                                                                                          <li>ternary-false: {{3 === '3' ? 'ano' : 'ne'}}</li>
 - výraz s globální proměnnou - globalVariable
   - vyzkoušejte i přístup přes window
     ... zdrojový kód TODO 2 -->                                                                                                                                                                                                                      <li>globalVariable: {{globalVariable}}</li>
                                                                                                          <li>globalVariable: {{globalVariable}}</li>
                                                                                                          <li>window.globalVariable: {{window.globalVariable}}</li>
   - všimněte si, že není možné k proměnné přistoupit, ale nedojde k žádné chybě
