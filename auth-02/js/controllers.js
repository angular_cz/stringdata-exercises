angular.module('authApp')

  .controller('AuthCtrl', function ($scope, $modalInstance, authService) {
    this.isBadLogin = false;

    $scope.$on('restApi:loginFailed', function () {
      this.isBadLogin = true;
    }.bind(this));

    this.loginUser = function () {
      authService.login("user", "password").then(function (auth) {
        $modalInstance.close(auth);
      });
    };

    this.loginAdmin = function () {
      authService.login("operator", "password").then(function (auth) {
        $modalInstance.close(auth);
      });
    };

    this.loginFail = function () {
      authService.login("badUser", "badPassword");
    };

    this.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  })

  .controller('OrderListController', function (Orders, authService, $scope) {
    var orderCtrl = this;
    this.auth = authService;
    this.orders = Orders.query();

    this.statuses = {
      NEW: 'Nová',
      CANCELLED: 'Zrušená',
      PAID: 'Zaplacená',
      SENT: 'Odeslaná'
    };

    this.removeOrder = function (order) {
      order.$remove(function () {
        var index = orderCtrl.orders.indexOf(order);
        orderCtrl.orders.splice(index, 1);
      });
    };

    this.reloadOrder = function (order) {
      var index = orderCtrl.orders.indexOf(order);
      orderCtrl.orders[index] = Orders.get({id: order.id});
    };

    this.updateOrder = function (order) {

      order.$save().catch( function() {
        //TODO 2.4 obnov záznam do původního stavu pomocí this.reloadOrder(order)
      });
    };
  })

  .controller('OrderDetailController', function (orderData) {
    this.order = orderData;
  })

  .controller('OrderCreateController', function (orderData, $location) {
    this.order = orderData;

    this.save = function () {
      this.order.$save(function () {
        $location.path("/orders");
      });
    };
  });