angular.module('authApp')
    .directive("authUserName", function(authService) {
      return {
        scope:{},
        restrict: "A",
        link: function(scope) {
          scope.auth = authService;
        },
        template: "{{auth.user.name}}"
      };
    })

    .directive("authLogoutLink", function(authService) {
      return {
        scope:{},
        restrict: "A",
        link: function(scope, element) {
          element.bind('click', authService.logout.bind(authService));
        }
      };
    })

    .directive("authIsAuthenticated", function(authService) {
      return {
        scope:{},
        restrict: "A",
        link: function(scope, element) {
          scope.auth = authService;

          element.toggleClass("ng-hide", !scope.auth.user)
          scope.$on("login:changedState", function() {
            element.toggleClass("ng-hide", !scope.auth.user);
          });
        }
      };
    });