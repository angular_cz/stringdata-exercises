Auth-02

Tato aplikace je modifikovanou variantou předchozího příkladu.

Api upravuje práva, tak abysme mohli jednoduše vyzkoušet reakci na stav 401 a 403.

Nepřihlášený uživatel může objednávky vidět.
Přihlášený uživatel může objednávky zakládat a měnit stav.
Přihlášený operátor může objednávky mazat.

Ovládací prvky jsou zobrazeny po celou dobu a přihlášení je nabídnuto až ve chvíli, kdy je potřeba uživatele autentizovat.
Přihlašovací stránku zde nahrazuje vyskakovací okno.

Cílem příkladu je rozšířit aplikaci o transparentní přihlášení v případě potřeby,
reakci na odepření přístupu, vyhodnocení oprávnění vrámci routeru a
zefektivnění použití direktivy pro kontrolu oprávnění

Postup :
--------

1. Podívejte se na aplikaci, můžete přejít na detail
  - v consoli se objeví 401, pokud se pokusíte objednávku změnit nebo smazat
  - v případě přidání se objeví 401 až při uložení.

2. Implementace zobrazení loginu
  - v interceptoru responseError (app.js) zobrazte v případě stavu 401 pomocí loginModal service
  - metoda vrací promise, definujte v then callback funkci, který se provede po úspěšném zavření modálního okna
  - v této metodě vraťte výsledek volání $http(response.config), který způsobí zopakování původního požadavku
  - nezapomeňte vrátit promise vracenou modálním oknem

  - podívejte se, jak se modální okno vytváří a na controller, který jej ovládá. (services.js, controllers.js, authModal.html)
  - všimněte si v okně reakce na špatné přihlášení

  - aby nedocházelo k otevírání modálního okna při špatném přihlášení, událost restApi:loginFailed pošlete
    pokud response.data.path === '/login'
    (response.data.path není součástí $http, tato cesta je součástí vráceného objektu chybě spring-security, můžete se podívat v consoli)

  - aby nedošlo k uložení změny stavu, pokud se uživatel nepřihlásí, ve zpracování změny
     v updateOrder (OrderListController, controllers.js) obnovte záznam do původního stavu pomocí reloadOrder

3. Vyzkoušejte, že pokud není uživatel přihlášen, je přihlášení zobrazeno :
  - když měníte stav
  - když ukládáte záznam
  - když mažete záznam

  - pokud přihlášení zrušíte, akce se neprovede
  - pokud se pokusíme smazat záznam přihlášeni jako uživatel, v consoli je vidět 403

  - pokud zvolíte špatné přihlášení, je zobrazena hlášk

4. Reakce na 403 - přístup odepřen
  - v interceptoru responseError přidejte větev pro  (response.status === 403)
  - zobrazte login pomocí volání metody prepareRejectModal

  - nezapomeňte vrátit promise vracenou modálním oknem

5. Vyzkoušejte, že přihlášený uživatel dostane po snaze smazat záznam chybovou hlášku v modálním okně

6. Kontrola oprávnění při přístupu na routu /create
  - v nastavení routeru pro /create (app.js) přidejte do části resolve určující závislostí, položku isAuthorized
    její hodnotou je funkce vracející logickou hodnotu,
    - uživatel je buť přihlášen - authService.isAuthenticated()
      nebo očekáváme přihlášení - loginModal.prepareLoginModal()
    - využijte vyhodnocování operátoru || v javascriptu
  - nezapomeňte na injektáž servis

7. Reakce routeru při nesplnění závislosti
  - router pošle zprávu $routeChangeError
  - v jejím zpracování (app.js) proveďte přesměrování na /

8. Vyzkoušejte, že je při vstupu na / create požadováno přihlášení

9. Stávající direktivy jsou řízeny pomocí watch, to nemusí být výhodné, přepracujeme je na události
  - odešlete zprávu "login:changedState" v místě přihlášení(checkLoginResponse) a odhlášení(logou) v authService
  - změňte v direktive authIsAuthenticated kontrolu pomocí $watch na čekání na zprávu "login:changedState"

  - ověřte správné chování