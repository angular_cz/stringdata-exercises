'use strict';

angular.module('filterApp', [])
  .controller('RealityCtrl', function ($http) {
    var reality = this;
    
    $http.get('cities.json').success(function(data){
      reality.cities = data;
    });
  });