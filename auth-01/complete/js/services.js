angular.module('authApp')

  .factory('Orders', function (REST_URI, $resource) {
    return $resource(REST_URI + '/orders/:id', {"id": "@id"});
  })

  .factory('authService', function ($http, REST_URI, $rootScope) {

    var successLogin = function (response) {
      auth.user = response.data;

      $http.defaults.headers.common['X-Auth-Token'] = response.data.token;

      $rootScope.$broadcast("login:loginSuccess");
    };

    var failedLogin = function (response) {
      $rootScope.$broadcast("login:loginFailed");
    }

    var auth = {
      user: null,
      login: function (name, pass) {
        return $http.post(REST_URI + "/login", {name: name, password: pass})
          .then(successLogin, failedLogin);
      },
      logout: function () {
        $http.post(REST_URI + "/logout").then(function () {
          auth.user = null;
          delete $http.defaults.headers.common['X-Auth-Token'];

          $rootScope.$broadcast("login:loggedOut")
        })
      },
      hasRole: function (role) {
        if (!this.user) {
          return false;
        }

        return this.user.roles.indexOf(role) !== -1;
      },
      mustHaveRole :function(role) {
        if (!this.hasRole(role)) {
          $rootScope.$broadcast("login:accessDenied")
        }
      }
    };

    return auth;
  });