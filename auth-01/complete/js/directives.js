angular.module('authApp')
  .directive("authUserName", function (authService) {
    return {
      scope: {},
      restrict: "A",
      link: function (scope) {
        scope.auth = authService;
      },
      template: "{{auth.user.name}}"
    };
  })

  .directive("authLogoutLink", function (authService) {
    return {
      scope: {},
      restrict: "A",
      link: function (scope, element) {
        element.bind('click', authService.logout.bind(authService));
      }
    };
  })

  .directive("authIsAuthenticated", function (authService) {
    return {
      scope: {},
      restrict: "A",
      link: function (scope, element) {
        scope.auth = authService;
        scope.$watch("auth.user", function (newValue, oldValue) {
          element.toggleClass("ng-hide", !scope.auth.user);
        });
      }
    };
  })

  .directive("authIsAnonymous", function (authService) {
    return {
      scope: {},
      restrict: "A",
      link: function (scope, element) {
        scope.auth = authService;
        scope.$watch("auth.user", function (newValue, oldValue) {
          element.toggleClass("ng-hide", scope.auth.user);
        });
      }
    };
  })

  .directive("authHasRole", function (authService) {
    return {
      scope: {
        role: "@authHasRole"
      },
      restrict: "A",
      link: function (scope, element) {
        scope.auth = authService;

        scope.$watch("auth.user", function () {
          var hasRole = authService.hasRole(scope.role);
          element.toggleClass("ng-hide", !hasRole);
        });
      }
    };
  });