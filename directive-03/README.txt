Directive-03

Cílem bude vytvořit direktivu, která umí zobrazovat hodnocení a jeho popis.
Poté vytvořit druhou, která takovéto direktivy umí obalit a vypočítat jejich průměr.

0. Vytvoření direktivy pro hodnocení
  - direktiva rating už je definovaná v app.js
  - podívejte se jak vypadá, jak je použitá v index.html a jak se vykreslí

1. Vícenásobné použití direktivy
  - použijeme direktivu také pro atributy food a place
    ... zdrojový kód TODO 1 - vícenásobné použití -->
                                                                                                          <li rating="rating.restaurant.food">Jídlo</li>
                                                                                                          <li rating="rating.restaurant.place">Místo</li>
2. Vytvoření obalující direktivy pro výpočet průměrného hodnocení

  - použijte direktivu average-rating na nadřazený div nad použití rating
  - a podívejte se, co se stane, pak pokračujte dalším bodem
    ... zdrojový kód TODO 2.1 - použití direktivy average-rating -->
                                                                                                          <div class="well" average-rating>

  - elementy obalené direktivou zmizely, zamyslete se proč
  - přidejte podporu transckluze do definičního objektu direktivy
  - vložte transcludovaný obsah pomocí příslušné direktivy do šablony average-rating.html
    ... zdrojový kód TODO 2.2 - aktivace transkluze -->
                                                                                                          transclude : true                                                                                                      <ng-transclude></ng-transclude>
3. Přidání controlleru
  - injektněte do definice této directivy v app.js service averageCounter
    ... zdrojový kód TODO 3.1 - injektněte averageCounter -->
                                                                                                          .directive("averageRating", function (averageCounter) {

  - přidejte direktivě controller
  - do kontrolleru přidejte metody addRating(rating), getAverage()
  - metody budou provolávat stejnojmenné metody service averageCounter
  - controllerAs nastavte na "average"
    ... zdrojový kód TODO 3.2 - vytvoření controlleru -->
                                                                                                          controller : function() {
                                                                                                            this.addRating = function(rating) {
                                                                                                              averageCounter.addRating(rating);
                                                                                                            };

                                                                                                            this.getAverage = function() {
                                                                                                              return averageCounter.getAverage();
                                                                                                            };
                                                                                                          },
                                                                                                          controllerAs: "average"
  - použijte metodu average.getAverage() v šabloně direktivy - average-rating.html
    ... zdrojový kód TODO 3.3 - zobrazení průměrné hodnoty -->
                                                                                                          <h2>Průměrné hodnocení - {{average.getAverage()}}%</h2>

4. Komunikace mezi direktivami
  - přidejte do direktivy rating závislost na averagerating
    ... zdrojový kód TODO 4.1 - přidejte závislost na averageRating -->
                                                                                                          require: "^averageRating"
  - přidejte parametr controller jako poslední parametr metody link
  - zavolejte na controlleru metodu addRating(rating)
    ... zdrojový kód TODO 4.2 - komunikace s averageRating -->
                                                                                                          link: function (scope, element, attr, controller) {
                                                                                                                  controller.addRating(scope.rating);
                                                                                                                  ...


5. Vyzkoušejte chování direktivy

EXTRA
  - vyzkoušejte použití direktivy mimo average counter
  - proč to nefunguje?

  - závislost by měla být podmíněná
  - proč to ani potom nefunguje?

  - je třeba vyřešit neexistenci controlleru v metodě link
