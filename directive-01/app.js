'use strict';

angular.module('directiveApp', [])
  .controller('RealityCtrl', function () {
    this.localities = [
      'Jihočeský',
      'Jihomoravský',
      'Karlovarský',
      'Královéhradecký',
      'Liberecký',
      'Moravskoslezský',
      'Olomoucký',
      'Pardubický',
      'Plzeňský',
      'Praha',
      'Středočeský',
      'Ústecký',
      'Vysočina',
      'Zlínský'
    ];
  })
  .controller('InfoCtrl', function () {
    var currentTab = 'about';

    this.setTab = function (tab) {
      currentTab = tab;
    };

    this.isTabActive = function (tab) {
      return currentTab === tab;
    };

    this.getActiveTab = function () {
      return currentTab;
    };
  });