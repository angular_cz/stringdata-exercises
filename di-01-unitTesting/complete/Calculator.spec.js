describe('calculator', function() {

  beforeEach(module('diApp'));

  it('should return 90 when format is A5 and number of pages is 0', function() {
    inject(function(calculator) {

    var product = {
      pageSize: 'A5',
      numberOfPages: 0
    };

    expect(calculator.getPrice(product)).toEqual(90);
    });
  });

  it('should return 120 when format is A5 and number of pages is 50', function() {
    inject(function(calculator) {

    var product = {
      pageSize: 'A5',
      numberOfPages: 50
    };

    expect(calculator.getPrice(product)).toEqual(120);
    });
  });

});