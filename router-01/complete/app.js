angular.module('orderAdministration', ['ngRoute'])
  .constant('REST_URI', 'http://api-angularcz.rhcloud.com/orders-api')
  .config(function($routeProvider) {

      $routeProvider
          .when('/orders', {
            templateUrl: 'orderList.html',
            controller: 'OrderListController',
            controllerAs: 'list'
          })
          .when('/detail/:id', {
            templateUrl: 'orderDetail.html',
            controller: 'OrderDetailController',
            controllerAs: 'detail',
            resolve: {
              orderData: function($http, $route, REST_URI) {
                return $http.get(REST_URI + '/orders/' + $route.current.params.id)
                    .then(function(response) {
                      return response.data;
                    });
              }
            }
          })
          .otherwise('/orders');
    })
    .controller('OrderListController', function($http, REST_URI) {
      this.orders = [];

      this.statuses = {
        NEW : 'Nová',
        MADE: 'Vyrobená',
        CANCELLED: 'Zrušená',
        PAID: 'Zaplacená',
        SENT: 'Odeslaná'
      };

      this.onOrdersLoad = function(orders) {
        this.orders = orders;
      };

      $http.get(REST_URI + '/orders')
          .then(function(response) {
            return response.data;
          })
          .then(this.onOrdersLoad.bind(this));

    })
    .controller('OrderDetailController', function(orderData) {
      this.order = orderData;
    });